libapache-admin-config-perl (0.95-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libapache-admin-config-perl: Add Multi-Arch:
    foreign.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 09 Dec 2022 18:08:24 +0000

libapache-admin-config-perl (0.95-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Alex Muntada ]
  * Bump debhelper-compat to 12
  * Set Rules-Require-Root: no
  * Declare compliance with Debian Policy 4.5.0
  * Add debian/upstream/metadata

 -- Alex Muntada <alexm@debian.org>  Sun, 05 Apr 2020 00:35:02 +0200

libapache-admin-config-perl (0.95-1) unstable; urgency=medium

  * Take over for the Debian Perl Group on maintainer's request
    (https://lists.debian.org/debian-perl/2017/08/msg00061.html)
  * debian/control:
    - Added: Vcs-Git field (source stanza); Vcs-Browser field (source
      stanza); Homepage field (source stanza); ${misc:Depends} to
      Depends: field.
    - Removed: Homepage pseudo-field (Description).
    - Changed: Maintainer set to Debian Perl Group <pkg-perl-
      maintainers@lists.alioth.debian.org> (was: Raphaël Pinson
      <raphink@ubuntu.com>).
  * debian/watch: fix and use MetaCPAN URL
  * debian/rules: switch to dh(1)
  * Bump debhelper compatibility level to 9
  * Switch to source format "3.0 (quilt)"
  * Enable autopkgtest
  * Declare compliance with Debian Policy 4.1.0
  * debian/copyright: switch formatting to Copyright-Format 1.0
  * Add myself to Uploaders
  * Shorten Description line length
  * New upstream version 0.95
  * Review patches after upgrade to 0.95

 -- Alex Muntada <alexm@debian.org>  Tue, 12 Sep 2017 18:31:19 +0200

libapache-admin-config-perl (0.94-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Apply patch by Niko Tyni to fix FTBFS with perl 5.10
    (Closes: #467122)

 -- Frank Lichtenheld <djpig@debian.org>  Wed, 09 Jul 2008 20:56:04 +0200

libapache-admin-config-perl (0.94-1) unstable; urgency=low

  * Initial release (Closes: #382381)

 -- Raphaël Pinson <raphink@ubuntu.com>  Thu, 10 Aug 2006 14:38:00 +0200
